//
//  TestViewController.swift
//  Test23
//
//  Created by sasha on 07.06.17.
//  Copyright © 2017 ZYR. All rights reserved.
//

import UIKit
import Alamofire

class TestViewController: UIViewController {

    var id: Int = 0
    var jsonArray: NSDictionary?
    var authHeader: NSDictionary = [:]
    var user: String = ""
    
    @IBOutlet weak var radioAnswerA: RadioButton!
    @IBOutlet weak var radioAnswerB: RadioButton!
    @IBOutlet weak var radioAnswerC: RadioButton!
    @IBOutlet weak var radioAnswerD: RadioButton!
    
    @IBOutlet weak var labelQuestion: UILabel!
    
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var labelCurrent: UILabel!
    
    @IBOutlet weak var navigationTitle: UINavigationItem!
    
    @IBOutlet weak var labelAnswerA: UILabel!
    @IBOutlet weak var labelAnswerB: UILabel!
    @IBOutlet weak var labelAnswerC: UILabel!
    @IBOutlet weak var labelAnswerD: UILabel!
    
    var countQuestion: Int = 0
    var currentQuestion: Int = 0
    var answer: Int = 0
    var question: String = ""
    var rightAnswerCount: Int = 0
    
    
// MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        // get all data
        Alamofire.request(.GET, "http://127.0.0.1:8000/tests/\(self.id)/", headers: self.authHeader as! [String : String])
            .responseJSON { response in
                debugPrint(response)
                if response.result.isSuccess {
                    self.jsonArray = response.result.value as? NSDictionary
                }
        }
    }
    
// MARK: - ViewDidAppear
    
    override func viewDidAppear(animated: Bool) {
        setTitle()
        setAnswer()
        setQuestion()
        setCount()
    }

// MARK: - SetLabelsTittleCountAnswerQuestion
    
    func setTitle(){
        self.navigationTitle.title = String(self.jsonArray!.objectForKey("title")!)
    }
    
    func setCount(){
        self.countQuestion = Int(self.jsonArray!.objectForKey("questions")!.count)
        self.labelCount.text = String(self.countQuestion)
        self.labelCurrent.text = String(self.currentQuestion + 1)
    }

    func setAnswer() {
        let labelAnswerMass = [self.labelAnswerA, self.labelAnswerB, self.labelAnswerC, self.labelAnswerD]
        for i in 0..<4 {
            let answer = self.jsonArray!.objectForKey("questions")!.objectAtIndex(self.currentQuestion).objectForKey("answer")!.objectForKey("answer\(i+1)")!
            labelAnswerMass[i].text = String(answer)
        }
        
    }
    
    func setQuestion()  {
        self.labelQuestion.text = String(self.jsonArray!.objectForKey("questions")!.objectAtIndex(self.currentQuestion).objectForKey("question")!)
        self.question = self.labelQuestion.text!
    }
    
// MARK: - NextQuestion
    
    @IBAction func nextQuestion(sender: AnyObject) {
        if (!checkSelected()) {
            return
        }
        self.answer = self.getSelectedAnswer()
        
        let param = ["question": self.question,
                     "answer": "\(self.answer)"]
        
        Alamofire.request(.POST, "http://127.0.0.1:8000/check/", headers: self.authHeader as! [String : String], parameters: param)
            .responseJSON { response in
                if response.result.isSuccess {
                    if let tmp: NSDictionary = (response.result.value as? NSDictionary)! {
                                        
                        if tmp.objectForKey("AnswerIsCorrect")!.isEqual("True"){
                            self.rightAnswerCount++
                        }
                    
                        self.currentQuestion++
                    
                        if self.currentQuestion < self.countQuestion {
                            self.setAnswer()
                            self.setQuestion()
                            self.setCount()
                        } else {
                            // complete test
                            self.performSegueWithIdentifier("finishView", sender: self.id)
                        }
                    
                        self.UnSelected()
                    }
                }
            }
    }
    
// MARK: - GetResult
    
    func getResult() -> String {
        // generere result
        return "\(self.rightAnswerCount)/\(self.countQuestion)"
    }
    
    
// MARK: - RadioButtonsFunctions
    
    @IBAction func clickedRadioAnswerA(sender: AnyObject) {
        self.UnSelected()
        self.radioAnswerA.selected = true
    }
    
    @IBAction func clickedRadioAnswerB(sender: AnyObject) {
        self.UnSelected()
        self.radioAnswerB.selected = true
    }
  
   
    @IBAction func clickedRadioAnswerC(sender: AnyObject) {
        self.UnSelected()
        self.radioAnswerC.selected = true
    }
    
    @IBAction func clickedRadioAnswerD(sender: AnyObject) {
        self.UnSelected()
        self.radioAnswerD.selected = true
    }
    
    func UnSelected()
    {
        self.radioAnswerA.selected = false
        self.radioAnswerB.selected = false
        self.radioAnswerC.selected = false
        self.radioAnswerD.selected = false
    }
    
    func checkSelected() -> Bool
    {
        if (self.radioAnswerA.selected || self.radioAnswerB.selected
            || self.radioAnswerC.selected || self.radioAnswerD.selected) {
                return true
        } else {
            return false
        }
    }
    
    func getSelectedAnswer() -> Int {
        if self.radioAnswerA.selected {
            return 1
        }
        if self.radioAnswerB.selected {
            return 2
        }
        if self.radioAnswerC.selected {
            return 3
        }
        if self.radioAnswerD.selected {
            return 4
        }
        return 0
    }
  

// MARK: - SEGUE
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "finishView" {
            let viewController = segue.destinationViewController as! FinishViewController
            viewController.authHeader = self.authHeader
            viewController.user = self.user
            viewController.rezult = self.getResult()
            viewController.title_test = String(self.jsonArray!.objectForKey("title")!)
        }
    }

}