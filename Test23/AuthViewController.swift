//
//  AuthViewController.swift
//  Test23
//
//  Created by sasha on 10.06.17.
//  Copyright © 2017 ZYR. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class AuthViewController: UIViewController {
    
    var token: String = ""

    @IBOutlet weak var labelUsername: UITextField!
    @IBOutlet weak var labelPassword: UITextField!
    @IBOutlet weak var labelError: UILabel!
    
// MARK: - LoginButtonTouch
    
    @IBAction func touchBtnLogin(sender: AnyObject) {
        
        if (self.labelPassword.text!.isEqual("") && self.labelUsername.text!.isEqual("")) {
            self.labelError.text = "Error! Empty login or password"
        }
        else {
            self.labelError.text = ""
            
            let param = ["username": self.labelUsername.text!,
                     "password": self.labelPassword.text!]
        
            Alamofire.request(.POST, "http://127.0.0.1:8000/get_auth_token/", parameters: param)
                .responseJSON { response in
                    if response.result.isSuccess {
                        
                        let tmp = response.result.value  as? NSDictionary
                        
                        if let val = tmp?.valueForKey("token") {
                            self.token = tmp?.valueForKey("token") as! String
                            self.labelError.text = ""
                        } else {
                            self.labelError.text = "Error! Check login or password"
                            return
                        }
                    }
                    else {
                        self.labelError.text = "Error 404. Not Found"
                        return
                    }
                    
                    self.performSegueWithIdentifier("testsView", sender: self)
            }
        }
        
    }
    
// MARK: - SEQUE
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "testsView" {
            let viewController = segue.destinationViewController as! ViewController
            let authHeader    = ["Authorization": "Token \(self.token)"]
            viewController.authHeader = authHeader
            viewController.user = self.labelUsername.text!
        }
    }
    
}


