//
//  FinishViewController.swift
//  Test23
//
//  Created by sasha on 11.06.17.
//  Copyright © 2017 ZYR. All rights reserved.
//

import UIKit
import Alamofire

class FinishViewController: UIViewController {
    
    var authHeader: NSDictionary = [:]
    var rezult: String = ""
    var title_test: String = ""
    var user: String = ""
    
    @IBOutlet weak var labelResult: UILabel!
    
// MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        self.labelResult.text = self.rezult
        
        let param = ["user": self.user,
                     "rezult": self.rezult,
                     "title_test": self.title_test]
        
        Alamofire.request(.POST, "http://127.0.0.1:8000/save_data/", parameters: param, headers: self.authHeader as! [String : String])
            .responseJSON { response in
                if response.result.isSuccess {
                    print(response.result.description)
                }
                else {
                    print(response.result.description)
                }
        }
    }
    
}
