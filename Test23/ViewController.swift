//
//  ViewController.swift
//  Test23
//
//  Created by sasha on 02.06.17.
//  Copyright © 2017 ZYR. All rights reserved.
//

import UIKit
import Alamofire


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var jsonArray:NSMutableArray?
    var newArray: Array<String> = []
    
    var id: Int = 0
    var authHeader: NSDictionary = [:]
    var user: String = ""

// MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        // set delegate
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        Alamofire.request(.GET, "http://127.0.0.1:8000/tests/", headers: self.authHeader as! [String : String])
            .responseJSON { response in
                if response.result.isSuccess {

                    if let JSON = response.result.value {
                        self.jsonArray = JSON as? NSMutableArray
                        //get all tests
                        for item in self.jsonArray! {
                            let string = item["title"]!
                            self.newArray.append(string! as! String)
                        }
                        self.tableView.reloadData()
                    }
                }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
// MARK: - DELEGATE FUNC
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = self.newArray[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        // Get ID test
        self.id = self.jsonArray!.valueForKey("id")[indexPath.row] as! Int
        self.performSegueWithIdentifier("testOpen", sender: self)
    }
    
// MARK: - SEQUE

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
            if segue.identifier == "testOpen" {
                let viewController = segue.destinationViewController as! TestViewController
                viewController.id = self.id
                viewController.authHeader = self.authHeader
                viewController.user = self.user
            }
    }

}

